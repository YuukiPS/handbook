FROM node:20-alpine

RUN yarn global add serve

WORKDIR /app

COPY . .

RUN yarn install
RUN yarn build

EXPOSE 8080

CMD ["serve", "dist", "-p", "8080"]