# Handbook Finder

Handbook Finder is a web application that allows users to search for various items from the GM Handbook. It's built with Vite, React and TypeScript, and uses Tailwind CSS for styling.


## Important Note

**This project has been discontinued and moved to [https://github.com/YuukiPS/Handbook](https://github.com/YuukiPS/Handbook).**

The new repository contains an updated version of the Handbook Finder, which I have personally developed into a cross-platform desktop and mobile application using Tauri, React, and TypeScript. As the developer, I've added exciting new features such as offline functionality, GM Handbook generation, and expanded support for both desktop and Android platforms.

I've also ensured that this project continues to support web browsers, though it's worth noting that the browser version has some limitations compared to the full desktop/mobile application. This decision was made to maintain accessibility while encouraging users to try the more feature-rich standalone versions.

## Features

-   Search functionality for various items
-   Copy ID functionality for each item
-   Dark mode for better user experience
-   Responsive design

## Getting Started

To get a local copy up and running, follow these steps:

1. Clone the repository:

    ```bash
    git clone https://gitlab.com/YuukiPS/handbook.git
    ```

2. Install the dependencies:

    ```bash
    yarn install or npm install
    ```

3. Start the development server:

    ```bash
    yarn dev or npm run dev
    ```

The application will be available at `http://localhost:5173`.

## Contributing

Contributions are welcome. Please open an issue first to discuss what you would like to change.

## License

This project is licensed under the MIT License. See the [LICENSE](./LICENSE) file for more details.

## Acknowledgements

-   This project uses the API from [api.elaxan.com](https://api.elaxan.com)
