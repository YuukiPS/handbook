import globals from 'globals';
import tseslint from 'typescript-eslint';
import react from 'eslint-plugin-react';
import reactRefresh from 'eslint-plugin-react-refresh';
import reactHooks from 'eslint-plugin-react-hooks';
import eslintPluginPrettier from 'eslint-plugin-prettier/recommended';

const tsEslint = tseslint.configs.recommended.map((config) => ({
    ...config,
    files: ['**/*.{ts,tsx}'],
}));

export default [
    { languageOptions: { globals: globals.browser } },
    ...tsEslint,
    eslintPluginPrettier,
    {
        files: ['**/*.tsx'],
        languageOptions: { parserOptions: { ecmaFeatures: { jsx: true } } },
        plugins: {
            'react-hooks': reactHooks,
        },
        rules: {
            'react-hooks/rules-of-hooks': 'error',
        },
    },
    {
        files: ['**/*.{tsx,jsx}'],
        plugins: {
            react,
        },
        languageOptions: {
            parserOptions: {
                ecmaFeatures: {
                    jsx: true,
                },
            },
            globals: {
                ...globals.browser,
            },
        },
        rules: {
            'react/jsx-uses-react': 'error',
            'react/jsx-uses-vars': 'error',
            'react/jsx-key': 'error',
            '@typescript-eslint/no-unused-vars': 'off',
            'no-console': ['error', { allow: ['warn', 'error'] }],
        },
    },
    {
        plugins: {
            'react-refresh': reactRefresh,
        },
    },
];
