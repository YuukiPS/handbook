import axios, { AxiosInstance } from 'axios';
import { MainData } from '@/types/gm';
import { Hsr } from '@/types/hsr';
import { Category } from '@/types/category';

interface HandbookGi {
    search: string;
    limit: number;
    category?: string;
    language?: string;
    command?: boolean;
    image?: boolean;
}

interface HandbookSr {
    search: string;
    limit: number;
    category?: string;
    language?: string;
}

class ElaXanAPI {
    private static readonly baseUrl = 'https://api.elaxan.com';

    private static readonly endpoints = {
        gi: '/v3/gm',
        sr: '/v1/sr',
        category: '/v2/category',
    };

    private static readonly instance: AxiosInstance = axios.create({
        timeout: 10_000,
        baseURL: this.baseUrl,
    });

    public static async getHandbook(
        type: 'gi',
        data: HandbookGi,
    ): Promise<MainData>;
    public static async getHandbook(type: 'sr', data: HandbookSr): Promise<Hsr>;
    public static async getHandbook(
        type: 'gi' | 'sr',
        data: HandbookGi | HandbookSr,
    ): Promise<MainData | Hsr> {
        const endpoint = this.endpoints[type];
        const payload = type === 'sr' ? { type: 1, ...data } : data;

        return this.instance
            .post<MainData | Hsr>(endpoint, payload)
            .then((res) => res.data);
    }

    public static async getCategoryList(type: 'gi' | 'sr'): Promise<Category> {
        const response = await this.instance.get(this.endpoints.category, {
            params: {
                type,
            },
        });

        return response.data;
    }
}

export default ElaXanAPI;
