import YuukiPS from '@/api/yuukips';
import React, {
    useCallback,
    useEffect,
    useMemo,
    useRef,
    useState,
} from 'react';
import { useCookies } from 'react-cookie';
import axios from 'axios';
import type { ListSuggestionCommands } from '@/types/listSuggestionCommands';
import Chat from './components/chat';
import Conversation from './components/Conversation';
import { useDebounce } from 'use-debounce';
import Tabs from './components/Tabs';
import {
    Dialog,
    DialogClose,
    DialogContent,
    DialogDescription,
    DialogFooter,
    DialogHeader,
    DialogTitle,
} from '@/components/ui/dialog';
import { Button } from '@/components/ui/button';
import { Kbd } from '@/components/ui/kbd';
import {
    ResizableHandle,
    ResizablePanel,
    ResizablePanelGroup,
} from '@/components/ui/resizable';
import { Link } from '@/components/ui/link';

const App: React.FC = () => {
    const [state, setState] = useState({
        command: '',
        disableChat: false,
        searchQuery: '',
        listCommand: [] as ListSuggestionCommands[],
        conversation: [
            {
                name: 'Bot',
                value: 'Type `/help` to see all command list',
                timestamp: new Date(),
                isBot: true,
            },
        ] as Conversation[],
        loading: false,
    });
    const [debouncedSearchQuery] = useDebounce(state.searchQuery, 300);
    const [cookies] = useCookies(['uid', 'code', 'server']);
    const wsRef = useRef<YuukiPS | null>(null);

    useEffect(() => {
        const yuukips = new YuukiPS();
        wsRef.current = yuukips;
        yuukips.getResponseCommand((response) => {
            addConversation(
                `${response.message.replace(/\t/g, '')} | ${response.retcode}`,
                'Bot',
                true,
            );
        });
    }, []);

    useEffect(() => {
        const fetchCommands = async () => {
            const response = await axios.post<ListSuggestionCommands[]>(
                'https://api.elaxan.com/v1/command-suggestion',
            );
            setState((prevState) => ({
                ...prevState,
                listCommand: response.data,
            }));
        };
        fetchCommands();
    }, []);

    const addConversation = useCallback(
        (message: string, name: string, isBot: boolean) => {
            setState((prevState) => ({
                ...prevState,
                conversation: [
                    ...prevState.conversation,
                    {
                        name,
                        value: message,
                        timestamp: new Date(),
                        isBot,
                    },
                ].slice(-100),
            }));
        },
        [],
    );

    const handleSendCommands = async (command: string) => {
        if (!wsRef.current) {
            addConversation(
                'Websocket connection not established',
                'Bot',
                true,
            );
            return;
        }
        addConversation(command, 'User', false);
        setState((prevState) => ({
            ...prevState,
            command: '',
        }));

        if (command === '/help') {
            const listCommands = state.listCommand
                .map(
                    (command) =>
                        `## ${command.name}\n${command.description}\n\`\`\`md\n${command.command}\n\`\`\``,
                )
                .join('\n');
            addConversation(listCommands, 'Bot', true);
            return;
        }
        setState((prevState) => ({
            ...prevState,
            disableChat: true,
        }));

        const listCommands = command.split('\n');

        try {
            setState((prevState) => ({
                ...prevState,
                loading: true,
            }));
            for await (const command of listCommands) {
                wsRef.current.sendCommand(
                    cookies.uid,
                    cookies.code,
                    cookies.server,
                    command,
                );
                if (listCommands.length > 1) {
                    await new Promise((resolve) => setTimeout(resolve, 3000));
                }
            }
        } catch (error) {
            console.error(error);
            addConversation(
                'An error occurred while processing the command',
                'Bot',
                true,
            );
        } finally {
            setState((prevState) => ({
                ...prevState,
                disableChat: false,
                loading: false,
            }));
        }
    };

    const getFilteredCommands = useCallback(
        (commands: ListSuggestionCommands[], searchQuery: string) =>
            commands.filter((command) => {
                const searchTerms = searchQuery.toLowerCase().split(' ');
                return searchTerms.every((term) =>
                    `${command.name} ${command.command} ${command.description}`
                        .toLowerCase()
                        .includes(term),
                );
            }),
        [],
    );

    const filteredCommands = useMemo(
        () => getFilteredCommands(state.listCommand, debouncedSearchQuery),
        [getFilteredCommands, state.listCommand, debouncedSearchQuery],
    );

    // const noConfiguration: boolean =
    //     !cookies.uid || !cookies.code || !cookies.server;
    const noConfiguration = false;

    return (
        <div style={{ height: 'calc(100vh - 4.25rem)' }}>
            {/* create a dialog that this page is outdated */}
            <Dialog open>
                <DialogContent>
                    <DialogTitle>This page is outdated</DialogTitle>
                    <DialogDescription>
                        This page is currently outdated, and we don't have plans
                        to update it in short term. Consider using the{' '}
                        <Link href='https://ps.yuuki.me/command'>
                            web command
                        </Link>{' '}
                        instead
                    </DialogDescription>
                    <DialogFooter>
                        <a href='https://ps.yuuki.me/command'>
                            <Button>Go to web command</Button>
                        </a>
                    </DialogFooter>
                </DialogContent>
            </Dialog>
            <ResizablePanelGroup
                direction='horizontal'
                className='rounded-lg border'
            >
                <ResizablePanel
                    defaultSize={30}
                    minSize={20}
                    maxSize={80}
                    className='hidden rounded-lg bg-gray-200 p-4 text-white shadow-lg dark:bg-gray-900 lg:block'
                >
                    <Tabs
                        filteredCommands={filteredCommands}
                        setCommand={(cmd) =>
                            setState((prevState) => ({
                                ...prevState,
                                command: cmd as string,
                            }))
                        }
                        searchQuery={state.searchQuery}
                        setSearchQuery={(query) =>
                            setState((prevState) => ({
                                ...prevState,
                                searchQuery: query as string,
                            }))
                        }
                    />
                </ResizablePanel>
                <ResizableHandle />
                <ResizablePanel className='flex flex-col'>
                    <Conversation conversation={state.conversation} />
                    <div className='m-auto mt-3 flex gap-1 text-xs text-gray-500 sm:flex sm:gap-2 sm:text-sm'>
                        <p className='hidden select-none text-sm lg:block'>
                            <Kbd>TAB</Kbd> to accept suggestions
                        </p>
                        <p className='hidden select-none lg:block'>
                            <Kbd className='mx-1'>CTRL</Kbd>
                            <Kbd>Enter</Kbd> to send command
                        </p>
                    </div>
                    <Chat
                        command={state.command}
                        setCommand={(cmd) =>
                            setState((prevState) => ({
                                ...prevState,
                                command: cmd as string,
                            }))
                        }
                        handleSendCommands={handleSendCommands}
                        disableChat={state.disableChat}
                        filteredCommands={filteredCommands}
                        searchQuery={state.searchQuery}
                        setSearchQuery={(query) =>
                            setState((prevState) => ({
                                ...prevState,
                                searchQuery: query as string,
                            }))
                        }
                        loading={state.loading}
                    />
                </ResizablePanel>
                {noConfiguration && (
                    <Dialog open={true}>
                        <DialogContent>
                            <DialogHeader>
                                <DialogTitle>
                                    Account not configured
                                </DialogTitle>
                                <DialogDescription>
                                    Please configure your account to use this
                                    feature.
                                </DialogDescription>
                            </DialogHeader>
                            <DialogClose asChild>
                                <Button
                                    onClick={() => {
                                        window.location.href = '/settings.html';
                                    }}
                                >
                                    Configure Now
                                </Button>
                            </DialogClose>
                        </DialogContent>
                    </Dialog>
                )}
            </ResizablePanelGroup>
        </div>
    );
};

export default App;
