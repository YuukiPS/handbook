import React, { useState, useEffect } from 'react';
import { RiSendPlaneLine } from 'react-icons/ri';
import type { ListSuggestionCommands } from '@/types/listSuggestionCommands';
import type { Autocomplete, List } from '@/types/autocompleteChat';
import axios from 'axios';
import { Textarea } from '@/components/ui/textarea';
import { Button } from '@/components/ui/button';
import OpenCommandsMobile from './openCommandsMobile';
import { Icon } from '@/components/ui/icon';

interface ChatProps {
    command: string;
    setCommand: React.Dispatch<React.SetStateAction<string>>;
    handleSendCommands: (command: string) => void;
    disableChat: boolean;
    filteredCommands: ListSuggestionCommands[];
    searchQuery: string;
    setSearchQuery: React.Dispatch<React.SetStateAction<string>>;
    loading: boolean;
}

const Chat: React.FC<ChatProps> = ({
    command,
    setCommand,
    handleSendCommands,
    disableChat,
    filteredCommands,
    searchQuery,
    setSearchQuery,
    loading,
}) => {
    const [filteredSuggestions, setFilteredSuggestions] = useState<List[]>([]);
    const [dataSuggestions, setDataSuggestions] = useState<Autocomplete | null>(
        null,
    );
    const [activeSuggestionIndex, setActiveSuggestionIndex] =
        useState<number>(0);
    const [showSuggestions, setShowSuggestions] = useState<boolean>(false);
    const [rows, setRows] = useState<number>(1);

    useEffect(() => {
        const getCommandList = async () => {
            const response = await axios
                .get<Autocomplete>('/command-list.json')
                .then((res) => res.data);

            setDataSuggestions(response);
        };

        getCommandList();
    }, []);

    useEffect(() => {
        if (!dataSuggestions) return;
        const suggestionMap = new Map(
            dataSuggestions.data.map((suggestion) => [
                suggestion.command.toLowerCase(),
                suggestion,
            ]),
        );
        const firstWord = command.split(' ')[0].toLowerCase();

        if (firstWord) {
            const filtered = Array.from(suggestionMap.keys())
                .filter((key) => key.startsWith(command.toLowerCase()))
                .map((key) => suggestionMap.get(key))
                .filter(
                    (suggestion): suggestion is List =>
                        suggestion !== undefined,
                );
            setFilteredSuggestions(filtered);
            setShowSuggestions(filtered.length > 0);
        } else {
            setShowSuggestions(false);
        }
    }, [command, dataSuggestions]);

    useEffect(() => {
        const lineCount = command.split('\n').length;
        setRows(lineCount);
    }, [command]);

    const handleKeyDown = (e: React.KeyboardEvent<HTMLTextAreaElement>) => {
        if (showSuggestions) {
            if (e.key === 'ArrowDown') {
                e.preventDefault();
                setActiveSuggestionIndex((prevIndex) =>
                    prevIndex === filteredSuggestions.length - 1
                        ? 0
                        : prevIndex + 1,
                );
            } else if (e.key === 'ArrowUp') {
                e.preventDefault();
                setActiveSuggestionIndex((prevIndex) =>
                    prevIndex === 0
                        ? filteredSuggestions.length - 1
                        : prevIndex - 1,
                );
            } else if (e.key === 'Tab') {
                e.preventDefault();
                if (filteredSuggestions.length > 0) {
                    setCommand(
                        filteredSuggestions[activeSuggestionIndex].command,
                    );
                    setShowSuggestions(false);
                }
            } else if (e.key === 'Escape') {
                setShowSuggestions(false);
            }
        }
        if (e.key === 'Enter' && e.ctrlKey && command.trim() !== '') {
            handleSendCommands(command);
        }
    };

    return (
        <div className='flex items-center'>
            <div
                className={`mt-auto flex items-center p-4 ${
                    disableChat && 'pointer-events-none'
                } w-full`}
            >
                <div className='lg:hidden'>
                    <OpenCommandsMobile
                        filteredCommands={filteredCommands}
                        setCommand={setCommand}
                        searchQuery={searchQuery}
                        setSearchQuery={setSearchQuery}
                    />
                </div>
                <div className='relative flex w-full'>
                    <Textarea
                        placeholder={`${
                            disableChat
                                ? 'Waiting for response...'
                                : 'Type here...'
                        }`}
                        className='input input-bordered w-full resize-none rounded-none focus:outline-none lg:rounded-l-lg'
                        onKeyDown={handleKeyDown}
                        onChange={(e) => setCommand(e.target.value)}
                        value={command}
                        ref={(textarea) => {
                            if (textarea) {
                                textarea.style.height = 'auto';
                                textarea.style.height = `${textarea.scrollHeight}px`;
                            }
                        }}
                        style={{
                            height: 'auto',
                            minHeight: '50px',
                            maxHeight: '200px',
                        }}
                        rows={rows}
                    />
                    {showSuggestions && (
                        <ul className='absolute bottom-full z-10 mb-1 max-h-60 w-full overflow-y-auto rounded-md border border-gray-300 bg-gray-800 shadow-lg'>
                            {filteredSuggestions.map((suggestion, index) => (
                                <li
                                    key={suggestion.command}
                                    className={`flex cursor-pointer items-center p-2 ${
                                        index === activeSuggestionIndex
                                            ? 'bg-gray-600'
                                            : 'bg-gray-700'
                                    }`}
                                    onMouseDown={() => {
                                        setCommand(suggestion.command);
                                        setShowSuggestions(false);
                                    }}
                                >
                                    <div className='flex flex-col'>
                                        <div className='mb-1 flex flex-row space-x-1'>
                                            {suggestion.type.map(
                                                (type, index) => (
                                                    <span
                                                        key={`suggestion-type-${index}`}
                                                        className='badge badge-neutral badge-sm items-center justify-center text-white'
                                                    >
                                                        {type}
                                                    </span>
                                                ),
                                            )}
                                        </div>
                                        <span className='text-white'>
                                            {suggestion.command}
                                        </span>
                                        <p className='text-sm opacity-35'>
                                            {suggestion.description}
                                        </p>
                                    </div>
                                </li>
                            ))}
                        </ul>
                    )}
                </div>
                <Button
                    className='h-auto self-stretch rounded-l-none bg-gray-500 dark:bg-gray-800'
                    style={{ minHeight: '51px' }}
                    onClick={() => {
                        if (command.trim() !== '') {
                            handleSendCommands(command);
                        }
                    }}
                    loading={loading}
                    type='submit'
                >
                    <Icon icon={RiSendPlaneLine} size={25} />
                </Button>
            </div>
        </div>
    );
};

export default Chat;
