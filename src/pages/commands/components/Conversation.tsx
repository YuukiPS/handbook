import type { Conversation } from '@/types/conversation';
import Markdown from 'react-markdown';
import remarkGfm from 'remark-gfm';
import rehypeRaw from 'rehype-raw';

interface ConversationProps {
    conversation: Conversation[];
}

const Conversation: React.FC<ConversationProps> = ({ conversation }) => {
    return (
        <div className='flex-grow overflow-auto'>
            {conversation.map((item) => (
                <div
                    className={`chat ${item.isBot ? 'chat-start' : 'chat-end'}`}
                    key={`conversation-${item.timestamp.getTime()}`}
                >
                    <div className={'chat-header flex gap-2'}>
                        <p className='select-none'>{item.name}</p>
                        <time className={'text-gray select-none opacity-50'}>
                            {item.timestamp.toLocaleTimeString()}
                        </time>
                    </div>
                    <div className={'chat-bubble'}>
                        <Markdown
                            remarkPlugins={[remarkGfm]}
                            rehypePlugins={[rehypeRaw]}
                            className={'prose'}
                        >
                            {item.value}
                        </Markdown>
                    </div>
                </div>
            ))}
        </div>
    );
};

export default Conversation;
