import type { ListSuggestionCommands } from '@/types/listSuggestionCommands';
import React, { memo } from 'react';
import Commands from './FilteredCommands';
import { Input } from '@/components/ui/input';
import {
    Tabs as TabsComponents,
    TabsContent,
    TabsList,
    TabsTrigger,
} from '@/components/ui/tabs';

interface TabsProps {
    filteredCommands: ListSuggestionCommands[];
    setCommand: React.Dispatch<React.SetStateAction<string>>;
    searchQuery: string;
    setSearchQuery: React.Dispatch<React.SetStateAction<string>>;
}

const Tabs: React.FC<TabsProps> = memo(
    ({ filteredCommands, setCommand, searchQuery, setSearchQuery }) => (
        <div className='mt-2 h-[calc(100vh-7rem)] space-y-4 overflow-y-auto p-1'>
            <h2 className='text-sm font-bold text-black opacity-50 dark:text-gray-300'>
                Suggested Commands
            </h2>
            <Input
                type='text'
                className='mt-2 w-full rounded-lg p-2 dark:bg-gray-800'
                placeholder='Search...'
                value={searchQuery}
                onChange={(e) => setSearchQuery(e.target.value)}
            />
            <TabsComponents defaultValue='GC'>
                <TabsList className='grid w-full grid-cols-3'>
                    <TabsTrigger value='GC'>GC</TabsTrigger>
                    <TabsTrigger value='GIO'>GIO</TabsTrigger>
                    <TabsTrigger value='LC'>LC</TabsTrigger>
                </TabsList>
                <TabsContent value='GC'>
                    <Commands
                        commands={filteredCommands}
                        type='gc'
                        setCommand={setCommand}
                    />
                </TabsContent>
                <TabsContent value='GIO'>
                    <Commands
                        commands={filteredCommands}
                        type='gio'
                        setCommand={setCommand}
                    />
                </TabsContent>
                <TabsContent value='LC'>
                    <Commands
                        commands={filteredCommands}
                        type='lc'
                        setCommand={setCommand}
                    />
                </TabsContent>
            </TabsComponents>
        </div>
    ),
);

export default Tabs;
