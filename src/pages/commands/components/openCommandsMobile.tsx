import {
    Drawer,
    DrawerContent,
    DrawerDescription,
    DrawerHeader,
    DrawerTitle,
    DrawerTrigger,
} from '@/components/ui/drawer';
import React from 'react';
import { GoCommandPalette } from 'react-icons/go';
import Tabs from './Tabs';
import { ListSuggestionCommands } from '@/types/listSuggestionCommands';
import { Icon } from '@/components/ui/icon';
import { FaPlus } from 'react-icons/fa';
import { MdContentCopy } from 'react-icons/md';

interface OpenCommandsMobileProps {
    filteredCommands: ListSuggestionCommands[];
    setCommand: React.Dispatch<React.SetStateAction<string>>;
    searchQuery: string;
    setSearchQuery: React.Dispatch<React.SetStateAction<string>>;
}

const OpenCommandsMobile: React.FC<OpenCommandsMobileProps> = ({
    filteredCommands,
    searchQuery,
    setCommand,
    setSearchQuery,
}) => {
    return (
        <Drawer direction='bottom' modal>
            <DrawerTrigger className='flex h-auto min-h-[51px] w-14 items-center justify-center self-stretch rounded-l-lg rounded-r-none bg-gray-500 dark:bg-gray-800'>
                <Icon icon={GoCommandPalette} size={25} />
            </DrawerTrigger>
            <DrawerContent>
                <DrawerHeader>
                    <DrawerTitle>List of Suggestions Commands</DrawerTitle>
                    <DrawerDescription>
                        Use the{' '}
                        <span className='inline-flex items-center'>
                            <Icon icon={FaPlus} size={15} />
                        </span>{' '}
                        button to add commands, and the{' '}
                        <span className='inline-flex items-center'>
                            <Icon icon={MdContentCopy} size={15} />
                        </span>{' '}
                        button to copy commands. You can also search the
                        commands.
                    </DrawerDescription>
                </DrawerHeader>
                <Tabs
                    filteredCommands={filteredCommands}
                    setCommand={setCommand}
                    searchQuery={searchQuery}
                    setSearchQuery={setSearchQuery}
                />
            </DrawerContent>
        </Drawer>
    );
};

export default OpenCommandsMobile;
