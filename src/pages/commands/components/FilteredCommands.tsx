import type { ListSuggestionCommands } from '@/types/listSuggestionCommands';
import { MdContentCopy } from 'react-icons/md';
import { FaPlus } from 'react-icons/fa6';
import { useToast } from '@/components/ui/use-toast';
import React, { memo } from 'react';
import { Button } from '@/components/ui/button';
import {
    Card,
    CardContent,
    CardDescription,
    CardFooter,
    CardHeader,
    CardTitle,
} from '@/components/ui/card';

interface FilteredCommandsProps {
    commands: ListSuggestionCommands[];
    type: 'gc' | 'gio' | 'lc';
    setCommand: React.Dispatch<React.SetStateAction<string>>;
}

const Commands: React.FC<FilteredCommandsProps> = memo(
    ({ commands, type, setCommand }) => {
        const { toast } = useToast();
        return (
            <div className='flex flex-col items-center justify-center gap-2'>
                {commands
                    .filter((command) => command.type === type)
                    .map((command, index) => (
                        <Card
                            className='w-full'
                            key={`commands-suggestion-${index}`}
                        >
                            <CardHeader>
                                <CardTitle>{command.name}</CardTitle>
                                <CardDescription>
                                    {command.description}
                                </CardDescription>
                            </CardHeader>
                            <CardContent>
                                <p
                                    className='break-words rounded-lg bg-slate-800 p-5'
                                    dangerouslySetInnerHTML={{
                                        __html: command.command,
                                    }}
                                ></p>
                                {commands.filter(
                                    (command) => command.type === type,
                                ).length === 0 && (
                                    <p className='text-gray-400'>
                                        No commands found for this category.
                                    </p>
                                )}
                            </CardContent>
                            <CardFooter className='flex justify-end space-x-1'>
                                <Button
                                    size={'sm'}
                                    onClick={() => {
                                        setCommand((previousCommand) => {
                                            return `${previousCommand ? `${previousCommand}\n${command.command}` : command.command}`;
                                        });
                                    }}
                                >
                                    <FaPlus />
                                </Button>
                                <Button
                                    size={'sm'}
                                    onClick={() => {
                                        navigator.clipboard
                                            .writeText(command.command)
                                            .then(() => {
                                                toast({
                                                    title: 'Copied',
                                                    description: `Command: ${command.command}`,
                                                });
                                            });
                                    }}
                                >
                                    <MdContentCopy />
                                </Button>
                            </CardFooter>
                        </Card>
                    ))}
            </div>
        );
    },
);

export default Commands;
