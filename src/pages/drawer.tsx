import { RxHamburgerMenu, RxExternalLink } from 'react-icons/rx';
import { FaSearch } from 'react-icons/fa';
import { GoCommandPalette } from 'react-icons/go';
import { IoMdSettings } from 'react-icons/io';
import { FaGitlab, FaGithub } from 'react-icons/fa6';
import { useTheme } from '@/components/theme-provider';
import { Button } from '@/components/ui/button';
import { FiSun, FiMoon } from 'react-icons/fi';
import { MdOutlineCommit } from 'react-icons/md';
import {
    DropdownMenu,
    DropdownMenuTrigger,
    DropdownMenuContent,
    DropdownMenuItem,
} from '@/components/ui/dropdown-menu';
import React, { memo, useMemo } from 'react';
import {
    Sheet,
    SheetContent,
    SheetFooter,
    SheetTrigger,
} from '@/components/ui/sheet';
import {
    NavigationMenu,
    NavigationMenuContent,
    NavigationMenuItem,
    NavigationMenuLink,
    NavigationMenuList,
    NavigationMenuTrigger,
} from '@/components/ui/navigation-menu';
import LanguageSwitcher from '@/components/LanguageSwitcher.tsx';
import { cn } from '@/lib/utils';
import { useTranslation } from 'react-i18next';
import { HiDotsHorizontal } from 'react-icons/hi';
import { Label } from '@/components/ui/label';
import '@/i18n';

interface Link {
    title: string;
    href: string;
    description: string;
}

const links: Link[] = [
    {
        title: 'search_id',
        href: '/',
        description: 'search_id_description',
    },
    {
        title: 'commands',
        href: '/commands.html',
        description: 'commands_description',
    },
    {
        title: 'settings',
        href: '/settings.html',
        description: 'settings_description',
    },
    {
        title: 'changelog',
        href: '/changelog.html',
        description: 'changelog_description',
    },
];

const ListItem = memo(
    React.forwardRef<
        React.ElementRef<'a'>,
        React.ComponentPropsWithoutRef<'a'>
    >(({ className, title, children, ...props }, ref) => {
        return (
            <li>
                <NavigationMenuLink asChild>
                    <a
                        ref={ref}
                        className={cn(
                            'block select-none space-y-1 rounded-md p-3 leading-none no-underline outline-none transition-colors hover:bg-accent hover:text-accent-foreground focus:bg-accent focus:text-accent-foreground',
                            className,
                        )}
                        {...props}
                    >
                        <div className='text-sm font-medium leading-none'>
                            {title}
                        </div>
                        <p className='line-clamp-2 text-sm leading-snug text-muted-foreground'>
                            {children}
                        </p>
                    </a>
                </NavigationMenuLink>
            </li>
        );
    }),
);

const Drawer: React.FC = memo(() => {
    const { t } = useTranslation('default', { keyPrefix: 'drawer' });
    const { setTheme } = useTheme();

    const memoizedLinks = useMemo(
        () =>
            links.map((link) => (
                <ListItem
                    key={link.href}
                    title={t(link.title)}
                    href={link.href}
                >
                    {t(link.description)}
                </ListItem>
            )),
        [t],
    );

    return (
        <>
            <div className='flex'>
                <div className='navbar flex w-full items-center'>
                    <div className='mx-4 flex-1'>
                        <div className='mr-2 flex-none lg:hidden'>
                            <Sheet>
                                <SheetTrigger asChild>
                                    <Button variant='outline'>
                                        <RxHamburgerMenu size={20} />
                                    </Button>
                                </SheetTrigger>
                                <SheetContent className='flex h-full flex-col justify-between'>
                                    <div>
                                        <a
                                            href='/'
                                            className='flex items-center justify-between p-2 transition-colors hover:bg-slate-200 dark:hover:bg-base-300'
                                        >
                                            <div className='flex items-center gap-2'>
                                                <FaSearch className='text-sm' />
                                                <span>{t('search_id')}</span>
                                            </div>
                                            <RxExternalLink
                                                className='text-lg'
                                                aria-label='Open external link'
                                            />
                                        </a>
                                        <a
                                            href='/commands.html'
                                            className='flex items-center justify-between p-2 transition-colors hover:bg-slate-200 dark:hover:bg-base-300'
                                        >
                                            <div className='flex items-center gap-2'>
                                                <GoCommandPalette className='text-sm' />
                                                <span>{t('commands')}</span>
                                            </div>
                                            <RxExternalLink
                                                className='text-lg'
                                                aria-label='Open external link'
                                            />
                                        </a>
                                        <a
                                            href='/settings.html'
                                            className='flex items-center justify-between p-2 transition-colors hover:bg-slate-200 dark:hover:bg-base-300'
                                        >
                                            <div className='flex items-center gap-2'>
                                                <IoMdSettings className='text-sm' />
                                                <span>{t('settings')}</span>
                                            </div>
                                            <RxExternalLink
                                                className='text-lg'
                                                aria-label='Open external link'
                                            />
                                        </a>
                                        <a
                                            href='/changelog.html'
                                            className='flex items-center justify-between p-2 transition-colors hover:bg-slate-200 dark:hover:bg-base-300'
                                        >
                                            <div className='flex items-center gap-2'>
                                                <MdOutlineCommit className='text-sm' />
                                                <span>{t('changelog')}</span>
                                            </div>
                                            <RxExternalLink
                                                className='text-lg'
                                                aria-label='Open external link'
                                            />
                                        </a>
                                    </div>
                                    <SheetFooter className='flex flex-col p-2'>
                                        <a
                                            href='https://gitlab.com/YuukiPS/handbook/'
                                            className='mb-2 flex items-center gap-2'
                                        >
                                            <FaGitlab />
                                            <p className='text-sm'>
                                                GitLab (Handbook Finder)
                                            </p>
                                        </a>
                                        <a
                                            href='https://github.com/ElaXan'
                                            className='mb-2 flex items-center gap-2'
                                        >
                                            <FaGithub />
                                            <p className='text-sm'>Github</p>
                                        </a>
                                        <p className='text-center text-sm'>
                                            Copyright © 2023-
                                            {new Date().getFullYear()} ElaXan
                                        </p>
                                    </SheetFooter>
                                </SheetContent>
                            </Sheet>
                        </div>
                        <a href='/' className='text-lg'>
                            Handbook Finder
                        </a>
                        <div className='relative ml-2 hidden lg:block'>
                            <NavigationMenu orientation='vertical'>
                                <NavigationMenuList>
                                    <NavigationMenuItem>
                                        <NavigationMenuTrigger className='text-lg'>
                                            {t('menu')}
                                        </NavigationMenuTrigger>
                                        <NavigationMenuContent className='absolute left-0 w-[400px]'>
                                            <ul className='grid gap-3 p-4 md:w-[400px] lg:w-[500px] lg:grid-cols-[.75fr_1fr]'>
                                                {memoizedLinks}
                                            </ul>
                                        </NavigationMenuContent>
                                    </NavigationMenuItem>
                                </NavigationMenuList>
                            </NavigationMenu>
                        </div>
                    </div>
                    <div>
                        <div className='lg:hidden'>
                            <DropdownMenu>
                                <DropdownMenuTrigger className='rounded-lg border border-border p-2'>
                                    <HiDotsHorizontal size={20} />
                                </DropdownMenuTrigger>
                                <DropdownMenuContent
                                    className='w-52 space-y-2'
                                    align='end'
                                >
                                    <Label className={'flex justify-center'}>
                                        {t('mobile_appearance')}
                                    </Label>
                                    <LanguageSwitcher />
                                    <DropdownMenu>
                                        <DropdownMenuTrigger asChild>
                                            <Button variant='outline' size='sm'>
                                                <FiSun className='h-[1.2rem] w-[1.2rem] rotate-0 scale-100 transition-all dark:-rotate-90 dark:scale-0' />
                                                <FiMoon className='absolute h-[1.2rem] w-[1.2rem] rotate-90 scale-0 transition-all dark:rotate-0 dark:scale-100' />
                                                <span className='sr-only'>
                                                    Toggle theme
                                                </span>
                                            </Button>
                                        </DropdownMenuTrigger>
                                        <DropdownMenuContent align='start'>
                                            <DropdownMenuItem
                                                onClick={() =>
                                                    setTheme('light')
                                                }
                                            >
                                                {t('theme_mode.light')}
                                            </DropdownMenuItem>
                                            <DropdownMenuItem
                                                onClick={() => setTheme('dark')}
                                            >
                                                {t('theme_mode.dark')}
                                            </DropdownMenuItem>
                                            <DropdownMenuItem
                                                onClick={() =>
                                                    setTheme('system')
                                                }
                                            >
                                                {t('theme_mode.system')}
                                            </DropdownMenuItem>
                                        </DropdownMenuContent>
                                    </DropdownMenu>
                                </DropdownMenuContent>
                            </DropdownMenu>
                        </div>
                        <div className='hidden lg:flex lg:items-center lg:space-x-2'>
                            <LanguageSwitcher />
                            <DropdownMenu>
                                <DropdownMenuTrigger asChild>
                                    <Button variant='outline' size='sm'>
                                        <FiSun className='h-[1.2rem] w-[1.2rem] rotate-0 scale-100 transition-all dark:-rotate-90 dark:scale-0' />
                                        <FiMoon className='absolute h-[1.2rem] w-[1.2rem] rotate-90 scale-0 transition-all dark:rotate-0 dark:scale-100' />
                                        <span className='sr-only'>
                                            Toggle theme
                                        </span>
                                    </Button>
                                </DropdownMenuTrigger>
                                <DropdownMenuContent align='end'>
                                    <DropdownMenuItem
                                        onClick={() => setTheme('light')}
                                    >
                                        {t('theme_mode.light')}
                                    </DropdownMenuItem>
                                    <DropdownMenuItem
                                        onClick={() => setTheme('dark')}
                                    >
                                        {t('theme_mode.dark')}
                                    </DropdownMenuItem>
                                    <DropdownMenuItem
                                        onClick={() => setTheme('system')}
                                    >
                                        {t('theme_mode.system')}
                                    </DropdownMenuItem>
                                </DropdownMenuContent>
                            </DropdownMenu>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
});

export default Drawer;
