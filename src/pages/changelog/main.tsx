import ReactDOM from 'react-dom/client';
import { BrowserRouter as Router } from 'react-router-dom';
import App from './App';
import '@/styles/index.css';
import Drawer from '../drawer';
import { ThemeProvider } from '@/components/theme-provider';
import './changelog.css';

ReactDOM.createRoot(document.getElementById('root')!).render(
    <Router>
        <ThemeProvider>
            <Drawer />
            <App />
        </ThemeProvider>
    </Router>,
);
