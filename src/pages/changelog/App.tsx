import React from 'react';
import Markdown, { Components } from 'react-markdown';
import remarkGfm from 'remark-gfm';
import rehypeRaw from 'rehype-raw';
import rehypeSlug from 'rehype-slug';
import rehypeExternalLinks from 'rehype-external-links';
import changelog from './changelog.md';
import { PhotoProvider, PhotoView } from 'react-image-previewer';
const ComponentsH2: Components['h2'] = ({ node, ...props }) => {
    const id = node?.properties?.id;
    return (
        <h2 {...props} className='group inline-block'>
            {id && (
                <a
                    href={`#${id}`}
                    className='underline-from-center -ml-2 mr-3 opacity-0 transition-opacity duration-300 group-hover:opacity-100'
                />
            )}
            {node?.children?.map((child, index) => (
                <React.Fragment key={index}>
                    {React.isValidElement(child)
                        ? child
                        : typeof child === 'object' && 'value' in child
                          ? String(child.value)
                          : String(child)}
                </React.Fragment>
            ))}
        </h2>
    );
};
const ComponentsAnchor: Components['a'] = ({ node, ...props }) => {
    const isInternalLink = props.href && props.href.startsWith('#');
    return (
        <a
            target={isInternalLink ? undefined : '_blank'}
            rel='noopener noreferrer'
            {...props}
        >
            {node &&
                node.children.map((child, index) => (
                    <React.Fragment key={index}>
                        {React.isValidElement(child)
                            ? child
                            : typeof child === 'object' && 'value' in child
                              ? String(child.value)
                              : String(child)}
                    </React.Fragment>
                ))}
        </a>
    );
};
const ComponentsImg: Components['img'] = ({ node, ...props }) => {
    return (
        <PhotoProvider>
            <PhotoView src={props.src}>
                <img src={props.src} {...props} className='cursor-pointer' />
            </PhotoView>
        </PhotoProvider>
    );
};

const App: React.FC = () => {
    return (
        <div className='flex min-h-screen w-full flex-col p-10'>
            <div className='w-full'>
                <Markdown
                    components={{
                        h2: ComponentsH2,
                        a: ComponentsAnchor,
                        img: ComponentsImg,
                    }}
                    remarkPlugins={[remarkGfm]}
                    rehypePlugins={[
                        rehypeRaw,
                        rehypeSlug,
                        [rehypeExternalLinks, { rel: 'noopener noreferrer' }],
                    ]}
                    className={'prose mb-14 max-w-full'}
                >
                    {changelog}
                </Markdown>
            </div>
        </div>
    );
};

export default App;
