# Changelog

For a complete changelog of the Handbook Finder project structure or code changes, please see the [Commit History](https://gitlab.com/YuukiPS/handbook/-/commits/main/?ref_type=HEADS)

## June 30, 2024

-   Disabled [commands](/commands.html) page for now, since it's outdated and not being maintained.

## June 29, 2024

-   Fixed an issue on the [search id](/) page where, after encountering an error and searching for items or characters again, the page would consistently display an error message.

## June 27, 2024

-   Added new validation messages for UID and code inputs in multiple languages.
-   Bumped up Grasscutter (GC) support to version 4.7.
-   Changed the category name from 'Avatars' to 'Characters' in our search results to match the new API naming - your favorite heroes are still there, just under a new label!
-   Spruced up the heading anchors in the changelog - they're now sleeker and easier to spot when you hover over them.
-   Fixed a pesky bug where the Commands checkbox wasn't working properly for Star Rail searches. Now it should behave as expected, showing or hiding commands based on your preference.

## June 23, 2024

-   Added support for changing language by clicking the language text in the top right corner. This feature is currently only available on the [settings](/settings.html) and [search id](/) page. Supported languages include:

    -   English
    -   Chinese
    -   Japanese
    -   Indonesian
    -   Russian
    -   Thai

    More languages will be added in future updates.

## June 22, 2024

-   Revamped the [Settings](/settings.html) page with a new card layout and improved UI components.

    Before: Settings page with a basic layout and minimal styling.

    <img src="/changelog/settings-page-redesigned-update-2024-06-22.png" alt="Old settings page" width="400">

    After: Sleek card-based design with improved form elements and clearer visual hierarchy.

    <img src="/changelog/settings-page-redesigned-update-2024-06-22-2.png" alt="New settings page" width="400">

## June 20, 2024

-   We've added a new feature on the [search id](/) page: you can now set the `Result` count by either typing a number directly or using a slider. We've kept the limit between 1 and 500 to ensure smooth performance.
-   Updated the card on the [Search ID](/) page for the game Star Rail to include mission results. This update includes the `Type` (in the header/mission name) and the `Next Mission` details.
-   We also implemented a WebSocket to [search id](/) page to apply a command and get the response in real-time.
-   On the [commands](/commands.html) page, updated the method to send and receive responses from the YuukiPS API using WebSocket for real-time communication.
-   On the [search id](/) page, trimmed the search input to resolve the issue where results were not found due to whitespace at the beginning or end.

## June 17, 2024

-   Support to preview image in [changelog](/changelog.html) page.
-   Updated the navigation menu for desktop view to use dropdown menus

    The old navigation menu:

    ![old](/changelog/navigation-link-desktop-update-2024-06-17.png)

    The new navigation menu:

    ![new](/changelog/navigation-link-desktop-update-2024-06-17-2.png)

-   Updated the text content of links on the [Changelog](/changelog.html) page. For example, the link [June 16, 2024](#june-16-2024) will not open in a new tab, but the link [home](/) will.
-   Fixed an issue on the [Search ID](/) page where the message `No result found` was not displayed when a search yielded no results from the API.

## June 16, 2024

-   Added a `#` symbol next to the headers on the [Changelog](/changelog.html) page.
-   Fixed an issue on the [Search ID](/) page for the game `Honkai: Star Rail` where searching for an item or other names always returned `No results found` or `Search for something.`. This occurred when the `category` was set to `All`.
    -   Thanks to [Thoronium](https://discord.com/users/210795755223384064) and [grizzly](https://discord.com/users/723386728345370645) for reporting this issue.
-   Added a changelog page that lists the changes and updates for Handbook Finder since [June 08, 2024](#june-08-2024).
-   Added a feature in the [Search ID](/) page to automatically set the game based on the player data you set. If you set player data to `Genshin Impact`, it will set the game to `Genshin Impact`. Similarly, if you set `LC` or `HSR`, it will set the game to `Star Rail`.
-   Fixed the `Server name not found` issue on the [applying commands](/) and [commands](/commands.html) pages. Whenever a user uses the `Add manual` dropdown in the [settings](/settings.html) page, a dialog will prompt them to enter their in-game UID.

## June 15, 2024

-   Corrected the issue where the "Add Manual Server" option on the [settings](/settings.html) page showed up when there was an error or failure in checking player data. Now, this option will be shown only when a user has successfully checked player data or no results are found.
-   Fixed an issue on the [settings](/settings.html) page where manually adding a server and then using the search bar would focus on the dropdown. Now, it will only focus on the search bar.
-   Updated the description on the [settings](/settings.html) page from `Settings for the UID and Code` to `Settings page for updating your UID and access code`.

## June 14, 2024

-   Added a feature to manually add a server on the [settings](/settings.html) page.
-   Fixed account verification issues in settings that were always asking you to enter player data or configure the account.
-   Updated navigation links to use the .html extension for consistent routing.
-   Enhanced performance and refreshed when sliding through the results on the [Search ID](/) page, saving to cookies after 1 second.
-   Changed the cursor style on the [Search ID](/) page from pointer to a left-to-right slide.

## June 08, 2024

-   Updated the [Search ID](/) page to use the v3 API from `api.elaxan.com` to get lists of characters, weapons, monsters, items, etc., to lower data usage.
-   Updated the API structure from `ps.yuuki.me/api/v2` for getting or looking up player information, running commands, and looking up tickets/queue on the [commands](/commands.html), [Search ID](/), and [settings](/settings.html) pages.
