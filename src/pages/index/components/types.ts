import { Find } from '@/types/gm';
import { Datum } from '@/types/hsr';
import YuukiPS from '@/api/yuukips';

export type CurrentType = 'Genshin Impact' | 'Star Rail';

export interface State {
    mainData: Find[];
    mainDataSR: Datum[];
    searchTerm: string;
    searchInputValue: string;
    loading: boolean;
    error: boolean;
    errorMessage: string;
    listCategory: string[];
    currentType: CurrentType;
    selectedCategory: string;
    showImage: boolean;
    showCommands: boolean;
    showCommandsSR: boolean;
    currentLimit: number;
    yuukips: YuukiPS | null;
    output: string[];
}
