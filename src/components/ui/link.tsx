import * as React from 'react';
import { cn } from '@/lib/utils';
import { cva } from 'class-variance-authority';

type Variant = 'default' | 'no-underline';

export interface LinkProps extends React.LinkHTMLAttributes<HTMLAnchorElement> {
    href: string;
    children?: React.ReactNode;
    className?: string;
    target?: string;
    variant?: Variant;
}

const LinkVariants = cva(
    'text-sm font-medium text-blue-500 underline hover:text-blue-800',
    {
        variants: {
            variant: {
                default:
                    'text-sm font-medium text-blue-500 underline hover:text-blue-800',
                'no-underline':
                    'text-sm font-medium text-blue-500 hover:text-blue-800 no-underline',
            },
        },
        defaultVariants: {
            variant: 'default',
        },
    },
);

const Link = React.forwardRef<HTMLAnchorElement, LinkProps>(
    ({ className, href, children, variant = 'default', ...props }, ref) => {
        return (
            <a
                ref={ref}
                href={href}
                className={cn(LinkVariants({ variant }), className)}
                {...props}
            >
                {children}
            </a>
        );
    },
);

Link.displayName = 'Link';

export { Link };
