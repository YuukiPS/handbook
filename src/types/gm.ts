// export type CommandCategory =
//     | 'Avatars'
//     | 'Artifacts'
//     | 'Monsters'
//     | 'Materials'
//     | 'Achievements'
//     | 'Quests'
//     | 'Scenes'
//     | 'Dungeons'
//     | 'Weapons';

interface Command {
    gc: {
        [key: string]: {
            name: string;
            value: string;
        };
    };
    gio: {
        [key: string]: {
            name: string;
            value: string;
        };
    };
}

interface List {
    id: number;
    command: Command | undefined;
}

export interface AvatarsList extends List {
    rarity: number;
    category: 'Characters';
    image:
        | {
              card: string;
              icon: string;
              side: string;
          }
        | undefined;
}

export interface AvatarsListV2 extends AvatarsList {
    name: string;
    description?: string;
}

export interface ArtifactsList extends List {
    rarity: number;
    image?: string;
    category: 'Artifacts';
}

export interface ArtifactsListV2 extends ArtifactsList {
    name: string;
    description?: string;
}

export interface MonstersList extends List {
    category: 'Monsters';
    image?: string;
}

export interface MonstersListV2 extends MonstersList {
    name: string;
    description?: string;
}

export interface MaterialsList extends List {
    category: 'Materials';
    rarity?: number;
    image?: string;
}

export interface MaterialsListV2 extends MaterialsList {
    name: string;
    description?: string;
}

export interface AchievementsList extends List {
    category: 'Achievements';
}

export interface AchievementsListV2 extends AchievementsList {
    name: string;
    description: string;
}

export interface QuestsList extends List {
    category: 'Quests';
    image: string | undefined;
}

export interface QuestsListV2 extends QuestsList {
    name: string;
    description?: string;
}

export interface ScenesList extends List {
    category: 'Scenes';
    type: 'Dungeon' | 'World' | 'Room' | 'Home';
}

export interface ScenesListV2 extends ScenesList {
    name: string;
}

export interface DungeonsList extends List {
    category: 'Dungeons';
}

export interface DungeonsListV2 extends DungeonsList {
    name: string;
    description?: string;
}

export interface WeaponsList extends List {
    rarity: number;
    image: string;
    category: 'Weapons';
}

export interface WeaponsListV2 extends WeaponsList {
    name: string;
    description?: string | undefined;
}

export interface MainData {
    name: string;
    description: string;
    author: string;
    data: (
        | AvatarsListV2
        | ArtifactsListV2
        | MonstersListV2
        | MaterialsListV2
        | AchievementsListV2
        | QuestsListV2
        | ScenesListV2
        | DungeonsListV2
        | WeaponsListV2
    )[];
}

export type Find =
    | AvatarsListV2
    | ArtifactsListV2
    | MonstersListV2
    | MaterialsListV2
    | AchievementsListV2
    | QuestsListV2
    | ScenesListV2
    | DungeonsListV2
    | WeaponsListV2;
