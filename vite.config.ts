import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react-swc';
import path from 'path';
import mpa from 'vite-plugin-multi-pages';
import { existsSync, lstatSync, readdirSync, renameSync, rm } from 'fs';

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [
        react(),
        mpa({
            scanDir: 'src/pages',
            defaultEntries: 'search',
        }),
        // Source: https://dev.to/onticdani/how-to-load-and-render-markdown-files-into-your-vite-react-app-using-typescript-26jm
        {
            name: 'markdown-loader',
            transform(code, id) {
                if (id.slice(-3) === '.md') {
                    return `export default ${JSON.stringify(code)}`;
                }
            },
        },
        {
            name: 'after-build',
            apply: 'build',
            enforce: 'post',
            closeBundle() {
                const pagesDir = 'dist/src/pages';
                readdirSync(pagesDir).forEach((folder) => {
                    const folderPath = path.join(pagesDir, folder);
                    if (lstatSync(folderPath).isDirectory()) {
                        const indexPath = path.join(folderPath, 'index.html');
                        if (existsSync(indexPath)) {
                            const pathToMove = `dist/${folder}.html`;
                            console.log(`${indexPath} -> ${pathToMove}`);
                            renameSync(indexPath, pathToMove);
                        } else {
                            console.log(
                                `${folder} does not have an index.html file`,
                            );
                        }
                    }
                });
                rm('dist/src', { recursive: true }, (err) => {
                    if (err) console.error(err);
                });
            },
        },
    ],
    appType: 'mpa',
    resolve: {
        alias: {
            '@/styles': '/src/styles',
            '@/types': '/src/types',
            '@/api': '/src/api',
            '@': path.resolve(__dirname, './src'),
        },
    },
});
